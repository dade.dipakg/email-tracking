## How To Start ?

### Prepare email_tracking

1. Create a virtual environment with pyenv or whatever you want
2. Activate the virtual environment
3. Execute the following command: pip install -r requirements.txt
4. Execute the migrations of django follow the command **python manage.py migrate**
5. Now, run the project: **python manage.py runserver**

### Run project in docker

- For the very first to initialize the project run -

```commandline
sudo docker-compose up --build
```

- Now to service will be available at 0.0.0.0:8000 Port we can run our endpoints directly

#### Endpoints

- To create email and event - http://localhost/email-event/
    - Method: POST
    - Payload:
    ```
   { "date": "12455", "type": "message.created", "metadata": { "message_id": "cjld2cjxh0000qzrmn831i7rn", "match_id": 123 } }
    ```
    - Response: status 201
    ```
    {"id":1,"date":"1970-01-01T03:27:35Z","state":"CREATED","event":{"id":1,"date":"1970-01-01T03:27:35Z","type":"message.created","metadata":{"message_id":"cjld2cjxh0000qzrmn831i7rn","match_id":123}}}
    ```
- To create job - http://localhost/create-job/
    - Method - POST
    - Payload:
    ```
     {"title":"Backend Engineer", "state":"OPEN", "opening_date":"2022-04-06", "closing_date":"2022-05-06"}
    ```
    - Response:
    ```
     {"id": 2, "title": "Backend Engineer", "state": "OPEN", "opening_date": "2022-04-06", "closing_date": "2022-05-06" }
    ```
  

- To create candidate - http://localhost/register-candidate/
  - Method - POST
  - Payload:
  ```
     {"name":"Aditya Shekokar", "email":"adityashekokar@gmail.com", "date":"2012-09-04 06:00" }
  ```
  - Response: status 201
  ```
     {"id":2,"name":"Aditya Shekokar","email":"adityashekokar@gmail.com","date":"2012-09-04T06:00:00Z"}
  ```
  
- To create match between candidate and job - http://localhost/create-match/
  - Method - POST
  - Payload:
  ```
     {"candidate":"1","job":"1","email":"1","state":"IN_PROGRESS","candidate_interest":"True"}
  ```
  - Response: status 201
  ```
     {"id":1,"candidate_interest":true,"state":"IN_PROGRESS","candidate":1,"job":1,"email":1}
  ```
  
- To fetch events on the basis of job and candidate - http://localhost/filter-events/
  - Method - GET
  - Payload:
  ```
    {"candidate_id":"1", "job_id":"1"}
  ```
  - Response: status 201
  ```
    [{"id":1,"date":"1970-01-01T03:27:35Z","type":"message.created","metadata":{"match_id":123,"message_id":"cjld2cjxh0000qzrmn831i7rn"}},{"id":1,"date":"1970-01-01T03:27:35Z","type":"message.created","metadata":{"match_id":123,"message_id":"cjld2cjxh0000qzrmn831i7rn"}}]
  ```