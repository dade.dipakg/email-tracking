from django.contrib import admin
from django.urls import path, include

from event_management.views import MangeEvent, MangeJob, RegisterCandidate, JobsAndCandidateMatch, FilterEvents

urlpatterns = [
    path('email-event/', MangeEvent.as_view(), name="email_event"),
    path('create-job/', MangeJob.as_view(), name="create_job"),
    path('register-candidate/', RegisterCandidate.as_view(), name="register_candidate"),
    path('create-match/', JobsAndCandidateMatch.as_view(), name="crate_match"),
    path('filter-events/', FilterEvents.as_view(), name="filter_events"),
]
