from django.db import models
from django.contrib.postgres.fields import JSONField


# Create your models here.

class BaseModel(models.Model):
    _created_at = models.DateTimeField(auto_now_add=True, null=True)
    _updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class Event(BaseModel):
    date = models.DateTimeField()
    type = models.CharField(max_length=100)
    metadata = JSONField()


class Email(BaseModel):
    Choices = (("CREATED", "CREATED"), ("BOUNCED", "BOUNCED"), ("RESPONDED", "RESPONDED"))
    date = models.DateTimeField()
    state = models.CharField(choices=Choices, default="CREATED", max_length=100)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="email_events")


class Job(models.Model):
    STATES = (
        ('OPEN', 'OPEN'),
        ('CLOSED', 'CLOSED')
    )
    title = models.CharField(max_length=200)
    state = models.CharField(max_length=200, choices=STATES)
    opening_date = models.DateField()
    closing_date = models.DateField()


class Candidate(BaseModel):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    date = models.DateTimeField()


class Match(BaseModel):
    STATES = (
        ('IN_PROGRESS', 'IN_PROGRESS'),
        ('REPLIED', 'REPLIED'),
        ('COMPLETED', 'COMPLETED')

    )
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE, related_name="candidate_match")
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name="job_match")
    state = models.CharField(choices=STATES, max_length=100)
    candidate_interest = models.BooleanField()
    email = models.ForeignKey(Email, on_delete=models.CASCADE, related_name="email_match")

    class Meta:
        unique_together = ('candidate', 'job')
