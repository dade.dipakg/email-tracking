from django.contrib import admin

# Register your models here.
from event_management.models import Event, Email

admin.site.register(Event)
admin.site.register(Email)
