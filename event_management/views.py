from datetime import datetime

from django.shortcuts import render

# Create your views here.
from rest_framework import generics
from rest_framework.response import Response

from event_management.models import Event, Email, Job, Candidate, Match
from event_management.serializers import EventSerializer, EmailSerializer, JobSerializer, CandidateSerializer, \
    MatchSerializer


class MangeEvent(generics.ListCreateAPIView):
    """
    This class includes methods to create or list event with email.
    """
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def post(self, request):
        """
        This methods is used to create event and email as per the given conditions.
        """
        try:
            data = request.data
            timestamp = data.pop("date")
            dt_object = datetime.fromtimestamp(int(timestamp))
            data["date"] = str(dt_object)
            serializer = EventSerializer(data=data)
            if not serializer.is_valid():
                return Response(serializer.errors, status=400)
            event = Event.objects.create(**serializer.data)
            event_type = event.type
            email_id = data.get('emailId')
            if email_id and (event_type != "message.created"):
                email = Email.objects.get(id=email_id)
            if event_type == "message.created":
                email = Email.objects.create(date=event.date, state="CREATED", event=event)
            if event_type == "message.replied":
                email.event = event
                email.state = "RESPONDED"
                email.save()
            if event_type == "message.bounced":
                email.event = event
                email.state = "BOUNCED"
                email.save()
            return Response(EmailSerializer(email).data)
        except Email.DoesNotExist as e:
            return Response({"error": str(e)}, status=404)
        except Exception as e:
            return Response({"error": str(e)}, status=500)


class MangeJob(generics.ListCreateAPIView):
    """
    This class includes methods to create and list jobs.
    """
    serializer_class = JobSerializer
    queryset = Job.objects.all()


class RegisterCandidate(generics.ListCreateAPIView):
    """
    This class includes methods to create and list candidates.
    """
    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()


class JobsAndCandidateMatch(generics.ListCreateAPIView):
    """
    This class includes methods to create and list match.
    """
    serializer_class = MatchSerializer
    queryset = Match.objects.all()


class FilterEvents(generics.ListAPIView):
    """
    This class includes get method to fetch event with respect to candidate or job.
    """

    def get(self, request):
        data = request.GET
        candidate_id = data.get("candidate_id")
        events = []
        if candidate_id:
            matches = Match.objects.filter(candidate__id=candidate_id)
            for match in matches:
                events.append(match.email.event)
        job_id = data.get("job_id")
        if job_id:
            matches = Match.objects.filter(candidate__id=job_id)
            for match in matches:
                events.append(match.email.event)
        return Response(EventSerializer(set(events), many=True).data)
