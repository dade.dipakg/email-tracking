from datetime import datetime

from rest_framework import serializers

from event_management.models import Event, Email, Job, Candidate, Match


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ("id", "date", "type", "metadata")


class EmailSerializer(serializers.ModelSerializer):
    event = EventSerializer()

    class Meta:
        model = Email
        fields = ('id', 'date', 'state', 'event')


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('id', 'title', 'state', 'opening_date', 'closing_date')


class CandidateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        fields = ("id", "name", "email", "date")


class MatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Match
        fields = ('id', "candidate_interest", "state", "candidate", "job", "email")
